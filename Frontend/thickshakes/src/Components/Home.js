import React, { useState, useEffect } from 'react';
import home1 from "../Images/home1.mp4";
import home2 from "../Images/home2.mp4";
import home3 from "../Images/home3.mp4";
import "../Css/home.css"

import {
  Carousel,
  CarouselItem,
  CarouselControl,
  CarouselIndicators,
  CarouselCaption,
} from 'reactstrap';

const items = [
  {
    src: home1,
    altText: 'Slide 1',
    caption: 'Slide 1',
  },
  {
    src: home2,
    altText: 'Slide 2',
    caption: 'Slide 2',
  },
  {
    src: home3,
    altText: 'Slide 3',
    caption: 'Slide 3',
  },
];



function Home(args) {
  const [activeIndex, setActiveIndex] = useState(0);
  const [animating, setAnimating] = useState(false);

  const next = () => {
    if (animating) return;
    const nextIndex = activeIndex === items.length - 1 ? 0 : activeIndex + 1;
    setActiveIndex(nextIndex);
  };

  const previous = () => {
    if (animating) return;
    const nextIndex = activeIndex === 0 ? items.length - 1 : activeIndex - 1;
    setActiveIndex(nextIndex);
  };

  const goToIndex = (newIndex) => {
    if (animating) return;
    setActiveIndex(newIndex);
  };

  useEffect(() => {
    const advanceToNextSlide = () => {
      const nextIndex = activeIndex === items.length - 1 ? 0 : activeIndex + 1;
      setActiveIndex(nextIndex);
    };

    const timeoutId = setTimeout(() => {
      advanceToNextSlide();
    }, 10000); 

    return () => clearTimeout(timeoutId); 
  }, [activeIndex]);

  const slides = items.map((item, index) => {
    return (
    
      <CarouselItem
        onExiting={() => setAnimating(true)}
        onExited={() => setAnimating(false)}
        key={index}
      >
        <video loop autoPlay playsInline src={item.src} type="video/mp4" className='video'></video>
        <CarouselCaption
         
        />
      </CarouselItem>
      
    );
  });

  return (
    <div className='homeouterdiv'>
<div className='carouselbody'>
      <Carousel
      activeIndex={activeIndex}
      next={next}
      previous={previous}
      {...args}
    >
      <CarouselIndicators
        items={items}
        activeIndex={activeIndex}
        onClickHandler={goToIndex}
      />
      {slides}
      <CarouselControl
        direction="prev"
        directionText="Previous"
        onClickHandler={previous}
      />
      <CarouselControl
        direction="next"
        directionText="Next"
        onClickHandler={next}
      />
    </Carousel>
     
    </div>
      <div className='homeheading'>
<h1>SHAKE IT</h1>
<h1>YOUR WAY</h1>
<pre>Tasty and Smoothie</pre>

<button>Order Now</button>
      </div>
    </div>
      
  );
}

export default Home;