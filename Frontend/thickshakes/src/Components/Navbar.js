import React from 'react'
import "../Css/navbar.css"
import { Link } from "react-router-dom"
// import navbar from "../Images/navbar.mp4"
import logo from "../Images/cameraNav.jpg"

function Navbar() {
    return (
        <div className="outerdiv">
            {/* <video loop autoPlay muted playsInline src={navbar}></video> */}
            <div className='inner1'>
                <Link to="/home"><h1>ThickShakes</h1></Link>
                <img src={logo} alt="Logo" />
            </div>
            <div className='inner2'>
                <Link to="/contact"  ><h5>Contactus</h5></Link>
                <Link to="/about"><h5>Aboutus</h5></Link>
                <Link to="/loginform"><h5>Login</h5></Link>
                <Link to="/register"><h5>Register</h5></Link>
            </div>
        </div>
    )
}

export default Navbar