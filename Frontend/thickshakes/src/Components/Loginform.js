import React, { useState } from 'react'
import "../Css/loginform.css"
import videobg from "../Images/login video.mp4"
// import emailbg from "../Images/email.png"
// import password from "../Images/password.png"
import axios from 'axios'
import { Link } from 'react-router-dom'
import { useNavigate } from 'react-router-dom';

function Loginform() {
  const navigate = useNavigate()
  const [formdata, setformdata] = useState({
    email: "",
    password: ""
  });

  const handleformdata = async (e) => {
    let { name, value } = e.target

    setformdata({
      ...formdata,
      [name]: value
    })  
  }

  const handleSubmit = async (e) => {
    e.preventDefault()
    alert(`${formdata.email} and ${formdata.password}  `)
    console.log(formdata)

    try {
      let response = await axios.post('http://localhost:3005/login/postlogin', formdata)
      console.log(response.data)

      if (response.data === "Login successfull") {
        navigate("/home")
      }

      if (response.data === "invalid password") {
        alert(`invalid password`)
        setformdata({
          email: "",
          password: ""
        })
        navigate("/loginform")
      }

      if (response.data === "User not found") {
        alert(`email doesnt exists`)
        setformdata({
          email: "",
          password: ""
        })
        navigate("/loginform")
      }
      }
    catch (err) {
        console.log(err)
      }

    }

  return (
      <div>

        <div className='bodydiv'>
          <video loop autoPlay playsinline src={videobg}   ></video>

          <div className='formdiv'>
            <form className="loginbox" onSubmit={handleSubmit}>
              <h1>Login</h1>
              <label>Email</label>
              <div className="inputs">
                {/* <img src={emailbg} alt="emailbg"></img> */}
                <input type="email" placeholder='Enter email' name="email" value={formdata.email}
                  onChange={handleformdata} required></input>
              </div>
              <label>Password</label>
              <div className="inputs">
              {/* <img src={password} alt="password"></img> */}
                <input type="password" placeholder='Enter password' name="password" value={formdata.password}
                  onChange={handleformdata} required></input>
              </div>

              <div className="checkbox">
                <input type="checkbox"></input>
                <label>RememberMe</label>
              </div>
              <button id="buttons" type="submit">Login</button>
              <Link to="/register"><p>Dont have an Account? Register</p></Link>
            </form>
          </div>
        </div>
      </div>
    )
  }

  export default Loginform