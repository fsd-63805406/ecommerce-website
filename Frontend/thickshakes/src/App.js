import './App.css';
import {BrowserRouter,Routes,Route} from "react-router-dom"
import Loginform from './Components/Loginform'
import Navbar from './Components/Navbar';
import Home from './Components/Home';
import Contactus from './Components/Contactus';
import Aboutus from './Components/Aboutus';
import Registrationform from './Components/Registrationform';


function App() {
  return (
    <div className='App'>
     <BrowserRouter>
    <Navbar/>
    <Routes>
    <Route path="/home" element={<Home/>} />
     <Route path="/loginform" element={<Loginform/>} />
     <Route path="/contact" element={<Contactus/>} />
     <Route path="/about" element={<Aboutus/>} />
     <Route path="/register" element={<Registrationform/>} />
    </Routes>
           </BrowserRouter>
    </div>
  );
}

export default App;